FROM mcr.microsoft.com/dotnet/core/runtime:3.1

COPY Application.Paylike/bin/Release/netcoreapp3.1/publish/ /App

WORKDIR /App

ENTRYPOINT ["dotnet", "Application.Paylike.dll"]