﻿namespace Application.Paylike.Commands.PaylikeMerchant
{
    using Application.Paylike.DTO;
    using Application.Paylike.Paylike.Clients;
    using ITUtil.Common.Base;
    using ITUtil.Common.Command;
    using System;

    public class AddUser : InsertCommand<AddUserDTO>
    {
        public AddUser() : base("Add user to merchant")
        {

        }

        public override void Execute(AddUserDTO user)
        {
            if (string.IsNullOrEmpty(user.Email))
            {
                throw new NullOrDefaultException("Email was not provided", "Email");
            }

            var client = new Merchant();
            var result = client.AddUser(user.MerchantId, user.Email);
            if (result.IsSuccessStatusCode == false)
            {
                throw new Exception("Error from paylike");
            }
        }
    }
}
