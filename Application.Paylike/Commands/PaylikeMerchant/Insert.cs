﻿namespace Application.Paylike.Commands.PaylikeMerchant
{
    using Application.Paylike.DTO;
    using Application.Paylike.Paylike.Clients;
    using ITUtil.Common.Base;
    using ITUtil.Common.Command;
    using System;

    public class Insert : GetCommand<InsertMerchantDTO, GetMerchantDTO>
    {
        public Insert() : base("Insert merchant, all are required except bank if it's a test", OperationType.Insert)
        {

        }

        public override GetMerchantDTO Execute(InsertMerchantDTO merchant)
        {
            if (string.IsNullOrEmpty(merchant.Name))
            {
                throw new NullOrDefaultException("Name was not provided", "Name");
            }

            if (string.IsNullOrEmpty(merchant.Descriptor))
            {
                throw new NullOrDefaultException("Descriptor was not provided", "Descriptor");
            }

            if (string.IsNullOrEmpty(merchant.Currency))
            {
                throw new NullOrDefaultException("Currency was not provided", "Currency");
            }

            if (string.IsNullOrEmpty(merchant.Website))
            {
                throw new NullOrDefaultException("Website was not provided", "Website");
            }

            if (string.IsNullOrEmpty(merchant.Email))
            {
                throw new NullOrDefaultException("Email was not provided", "Email");
            }

            if (merchant.Company == null)
            {
                throw new NullOrDefaultException("Company was not provided", "Company");
            }

            if (merchant.Bank == null && merchant.Test == false)
            {
                throw new NullOrDefaultException("Bank was not provided", "Bank");
            }

            var client = new Client(merchant.AppKey);
            var mClient = new Merchant(client);
            var result = mClient.Create(new Paylike.Clients.PaylikeModels.Merchant(merchant));
            if (result.IsSuccessStatusCode == false)
            {
                throw new Exception("Error from paylike: " + result.ErrorMessage);
            }
            return new GetMerchantDTO(result.Result);
        }
    }
}
