﻿namespace Application.Paylike.Commands.PaylikeMerchant
{
    using ITUtil.Common.Command;
    using System.Collections.Generic;

    public class Namespace : BaseNamespace
    {
        public Namespace() : base("PaylikeMerchant", new ProgramVersion("1.0.0.0"))
        {
            this.Commands.AddRange(new List<ICommandBase>()
            {
                new Insert(),
                new GetAll(),
                new Get()
            });
        }
    }
}
