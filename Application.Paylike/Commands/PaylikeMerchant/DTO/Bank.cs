﻿namespace Application.Paylike.Commands.PaylikeMerchant.DTO
{
    public class Bank
    {
        public string IBAN { get; set; }

        public Bank() { }

        public Bank(Paylike.Clients.PaylikeModels.Bank bank)
        {
            this.IBAN = bank.IBAN;
        }
    }
}
