﻿namespace Application.Paylike.Commands.PaylikeMerchant.DTO
{
    public class Company
    {
        public string Country { get; set; }

        public string Number { get; set; }

        public Company() { }

        public Company(Paylike.Clients.PaylikeModels.Company c)
        {
            this.Country = c.Country;
            this.Number = c.Number;
        }
    }
}
