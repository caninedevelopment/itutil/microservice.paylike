﻿using Application.Paylike.Paylike.Clients.PaylikeModels;

namespace Application.Paylike.Commands.PaylikeMerchant.DTO
{
    public class ResponseGet
    {
        public string Id { get; set; }

        public string Key { get; set; }

        public string Name { get; set; }

        public string Currency { get; set; }

        public bool Test { get; set; }

        public string Email { get; set; }

        public string Website { get; set; }

        public string Descriptor { get; set; }

        public Company Company { get; set; }

        public Bank Bank { get; set; }

        public ResponseGet(Merchant result)
        {
            this.Id = result.Id;
            this.Key = result.Key;
            this.Name = result.Name;
            this.Currency = result.Currency;
            this.Test = result.Test;
            this.Email = result.Email;
            this.Website = result.Website;
            this.Descriptor = result.Descriptor;
            this.Company = new Company(result.Company);
            this.Bank = new Bank(result.Bank);
        }
    }
}
