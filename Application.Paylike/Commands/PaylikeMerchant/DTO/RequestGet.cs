﻿namespace Application.Paylike.Commands.PaylikeMerchant.DTO
{
    public class RequestGet
    {
        public string AppKey { get; set; }
        public string Id { get; set; }
    }
}
