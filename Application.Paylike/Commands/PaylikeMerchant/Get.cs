﻿namespace Application.Paylike.Commands.PaylikeMerchant
{
    using Application.Paylike.Commands.PaylikeMerchant.DTO;
    using Application.Paylike.Paylike.Clients;
    using ITUtil.Common.Base;
    using ITUtil.Common.Command;
    using System;

    public class Get : GetCommand<RequestGet, ResponseGet>
    {
        public Get() : base("Get merchant by id")
        {

        }

        public override ResponseGet Execute(RequestGet input)
        {
            if (string.IsNullOrEmpty(input.Id))
            {
                throw new NullOrDefaultException("AppKey was not provided", "AppKey");
            }

            if (string.IsNullOrEmpty(input.AppKey))
            {
                throw new NullOrDefaultException("AppKey was not provided", "AppKey");
            }

            var client = new Client(input.AppKey);
            var mClient = new Merchant(client);
            var result = mClient.Get(input.Id);
            if (result.IsSuccessStatusCode == false)
            {
                throw new Exception("Error from paylike: " + result.ErrorMessage);
            }
            return new ResponseGet(result.Result);
        }
    }
}
