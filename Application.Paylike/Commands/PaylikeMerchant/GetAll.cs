﻿namespace Application.Paylike.Commands.PaylikeMerchant
{
    using Application.Paylike.DTO;
    using Application.Paylike.Paylike.Clients;
    using ITUtil.Common.Base;
    using ITUtil.Common.Command;
    using System;

    public class GetAll : GetCommand<GetAllMerchantDTO, GetAllMerchantResponse>
    {
        public GetAll() : base("Get all merchants")
        {

        }

        public override GetAllMerchantResponse Execute(GetAllMerchantDTO input)
        {
            if (string.IsNullOrEmpty(input.AppKey))
            {
                throw new NullOrDefaultException("AppKey was not provided", "AppKey");
            }

            if (string.IsNullOrEmpty(input.AppId))
            {
                throw new NullOrDefaultException("AppId was not provided", "AppId");
            }

            var client = new Client(input.AppKey);
            var mClient = new Merchant(client);
            var result = mClient.GetAll(input.AppId, input.Limit.HasValue ? input.Limit.Value : 100);
            if (result.IsSuccessStatusCode == false)
            {
                throw new Exception("Error from paylike: " + result.ErrorMessage);
            }
            return new GetAllMerchantResponse(result.Result);
        }
    }
}
