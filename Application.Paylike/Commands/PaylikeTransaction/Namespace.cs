﻿namespace Application.Paylike.Commands.PaylikeTransaction
{
    using ITUtil.Common.Command;
    using System.Collections.Generic;

    public class Namespace : BaseNamespace
    {
        public Namespace() : base("PaylikeTransaction", new ProgramVersion("1.0.0.0"))
        {
            this.Commands.AddRange(new List<ICommandBase>()
            {
                new Insert(),
                new GetAll()
            });
        }
    }
}
