﻿namespace Application.Paylike.Commands.PaylikeTransaction
{
    using Application.Paylike.DTO;
    using Application.Paylike.Paylike.Clients;
    using ITUtil.Common.Base;
    using ITUtil.Common.Command;
    using System;

    public class GetAll : GetCommand<GetAllTransactionsRequest, GetAllTransactionsResponse>
    {
        public GetAll() : base("Get all transaction, on merchant")
        {

        }

        public override GetAllTransactionsResponse Execute(GetAllTransactionsRequest input)
        {
            if (string.IsNullOrEmpty(input.AppKey))
            {
                throw new NullOrDefaultException("AppKey was not provided", "AppKey");
            }

            if (string.IsNullOrEmpty(input.MerchantId))
            {
                throw new NullOrDefaultException("MerchantId was not provided", "MerchantId");
            }

            var client = new Client(input.AppKey);
            var tClient = new Transaction(client);
            var result = tClient.GetAll(input.MerchantId, (input.Limit.HasValue && input.Limit.Value > 0 ? input.Limit.Value : 100));
            if (result.IsSuccessStatusCode == false)
            {
                throw new Exception("Error from paylike: " + result.ErrorMessage);
            }
            return new GetAllTransactionsResponse(result.Result);
        }
    }
}
