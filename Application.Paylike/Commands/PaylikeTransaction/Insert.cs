﻿namespace Application.Paylike.Commands.PaylikeTransaction
{
    using Application.Paylike.DTO;
    using Application.Paylike.Paylike.Clients;
    using ITUtil.Common.Base;
    using ITUtil.Common.Command;
    using System;

    public class Insert : GetCommand<InsertTransactionDTO, GetTransactionDTO>
    {
        public Insert() : base("Create transaction", OperationType.Insert)
        {

        }

        public override GetTransactionDTO Execute(InsertTransactionDTO trans)
        {
            if (string.IsNullOrEmpty(trans.MerchantId))
            {
                throw new NullOrDefaultException("MerchantId was not provided", "MerchantId");
            }

            if (string.IsNullOrEmpty(trans.Descriptor))
            {
                throw new NullOrDefaultException("Descriptor was not provided", "Descriptor");
            }

            if (string.IsNullOrEmpty(trans.Currency))
            {
                throw new NullOrDefaultException("Currency was not provided", "Currency");
            }

            if (string.IsNullOrEmpty(trans.CardId))
            {
                throw new NullOrDefaultException("CardId was not provided", "CardId");
            }

            if (trans.AmountAsMinor < 1)
            {
                throw new NullOrDefaultException("AmountAsMinor needs to be higher than 0", "AmountAsMinor");
            }

            var client = new Client(trans.AppKey);
            var transaction = new Transaction(client);
            var result = transaction.Create(trans.MerchantId, trans.CardId, trans.Descriptor, trans.Currency, trans.AmountAsMinor);

            if (result.IsSuccessStatusCode == false)
            {
                throw new Exception("Error from paylike: " + result.ErrorMessage);
            }
            return new GetTransactionDTO(result.Result);
        }
    }
}
