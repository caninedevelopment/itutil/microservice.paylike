﻿namespace Application.Paylike.Commands.PaylikeCard
{
    using Application.Paylike.DTO;
    using Application.Paylike.Paylike.Clients;
    using ITUtil.Common.Base;
    using ITUtil.Common.Command;
    using System;

    public class Get : GetCommand<GetCardDTO, CardDTO>
    {
        public Get() : base("Get card by card id, and app key. Both are required")
        {

        }

        public override CardDTO Execute(GetCardDTO input)
        {
            if (string.IsNullOrEmpty(input.CardId))
            {
                throw new NullOrDefaultException("CardId was not provided", "CardId");
            }

            if (string.IsNullOrEmpty(input.AppKey))
            {
                throw new NullOrDefaultException("AppKey was not provided", "AppKey");
            }

            var client = new Client(input.AppKey);
            var cClient = new Card(client);
            var result = cClient.Get(input.CardId);
            if (result.IsSuccessStatusCode == false)
            {
                throw new Exception("Error from paylike");
            }

            return new CardDTO(result.Result);
        }
    }
}