﻿namespace Application.Paylike.Commands.PaylikeCard
{
    using ITUtil.Common.Command;
    using System.Collections.Generic;

    public class Namespace : BaseNamespace
    {
        public Namespace() : base("PaylikeCard", new ProgramVersion("1.0.0.0"))
        {
            this.Commands.AddRange(new List<ICommandBase>()
            {
                new Save(),
                new Get()
            });
        }
    }
}
