﻿namespace Application.Paylike.Commands.PaylikeCard
{
    using Application.Paylike.DTO;
    using Application.Paylike.Paylike.Clients;
    using ITUtil.Common.Base;
    using ITUtil.Common.Command;
    using System;

    public class Save : GetCommand<SaveCardDTO, CardDTO>
    {
        public Save() : base("Save card, save card from a transaction, via merchantid and transaction id. All is required.", OperationType.Insert)
        {
        }

        public override CardDTO Execute(SaveCardDTO info)
        {
            if (string.IsNullOrEmpty(info.TransactionId))
            {
                throw new NullOrDefaultException("TransactionId was not provided", "TransactionId");
            }

            if (string.IsNullOrEmpty(info.MerchantId))
            {
                throw new NullOrDefaultException("MerchantId was not provided", "MerchantId");
            }

            var client = new Card();
            var result = client.Save(info.TransactionId, info.MerchantId);
            if (result.IsSuccessStatusCode == false)
            {
                throw new Exception("Error from paylike");
            }
            return new CardDTO(result.Result);
        }
    }
}
