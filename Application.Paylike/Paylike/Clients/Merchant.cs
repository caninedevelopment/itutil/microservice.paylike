﻿namespace Application.Paylike.Paylike.Clients
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public class Merchant
    {
        private readonly Client _client;

        public Merchant(Client client = null)
        {
            if (client == null)
            {
                client = new Client();
            }

            _client = client;
        }

        public PaylikeApiResponse<PaylikeModels.Merchant> Get(string id)
        {
            return _client.MakePaylikeApiRequest<PaylikeModels.Merchant>($"merchants/{id}").Result;
        }

        public PaylikeApiResponse<List<PaylikeModels.Merchant>> GetAll(string appId, int limit = 100)
        {
            return _client.MakePaylikeApiRequest<List<PaylikeModels.Merchant>>($"identities/{appId}/merchants?limit={limit}", null).Result;
        }

        public PaylikeApiResponse<PaylikeModels.Merchant> Create(PaylikeModels.Merchant source)
        {
            return _client.MakePaylikeApiRequest<PaylikeModels.Merchant>($"merchants", source).Result;
        }

        public async Task<PaylikeApiResponse<PaylikeModels.Merchant>> Update(string merchantId, string name, string email, string descriptor)
        {
            var source = new { name, email, descriptor };
            return await _client.MakePaylikeApiRequest<PaylikeModels.Merchant>($"merchants/{merchantId}", source);
        }

        public PaylikeApiResponse<PaylikeModels.Merchant> AddUser(string merchantId, string email)
        {
            var source = new { email };
            return _client.MakePaylikeApiRequest<PaylikeModels.Merchant>($"merchants/{merchantId}/users", source).Result;
        }

        public async Task<PaylikeApiResponse<PaylikeModels.Merchant>> RemoveUser(string merchantId, string userId)
        {
            return await _client.MakePaylikeApiRequest<PaylikeModels.Merchant>($"merchants/{merchantId}/users/{userId}", null, true);
        }
    }
}
