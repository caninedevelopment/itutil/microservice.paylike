﻿namespace Application.Paylike.Paylike.Clients
{
    using System.Threading.Tasks;

    public class Card
    {
        private readonly Client _client;

        public Card(Client client = null)
        {
            if (client == null)
            {
                client = new Client();
            }

            _client = client;
        }

        public PaylikeApiResponse<PaylikeModels.Card> Get(string cardId)
        {
            var call = _client.MakePaylikeApiRequest<PaylikeModels.Card>($"cards/{cardId}");
            var result = call.Result;
            return result;
        }

        public PaylikeApiResponse<PaylikeModels.Card> Save(string transactionId, string merchantId)
        {
            var source = new { transactionId };
            var call = _client.MakePaylikeApiRequest<PaylikeModels.Card>($"merchants/{merchantId}/cards", source);
            var result = call.Result;
            return result;
        }
    }
}
