﻿namespace Application.Paylike.Paylike.Clients
{
    using Application.Paylike.Paylike.Utilities;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public class Transaction
    {
        private readonly Client _client;

        public Transaction(Client client = null)
        {
            if (client == null)
            {
                client = new Client();
            }

            _client = client;
        }

        public async Task<PaylikeApiResponse<PaylikeModels.Transaction>> Get(string transactionId)
        {
            return await _client.MakePaylikeApiRequest<PaylikeModels.Transaction>($"transactions/{transactionId}");
        }

        public PaylikeApiResponse<List<PaylikeModels.Transaction>> GetAll(string merchantId, int limit = 100)
        {
            return _client.MakePaylikeApiRequest<List<PaylikeModels.Transaction>>($"merchants/{merchantId}/transactions?limit={limit}").Result;
        }

        public PaylikeApiResponse<PaylikeModels.Transaction> Create(string merchantId, string cardId, string descriptor, string currency, int amountAsMinor)
        {
            var source = new { cardId, descriptor, currency, amount = amountAsMinor };
            return _client.MakePaylikeApiRequest<PaylikeModels.Transaction>($"merchants/{merchantId}/transactions", source).Result;
        }

        public PaylikeApiResponse<PaylikeModels.Transaction> Create(string merchantId, string cardId, string descriptor, string currency, decimal amountAsMajor)
        {
            return Create(merchantId, cardId, descriptor, currency, amountAsMajor.ToMinorUnits(currency));
        }
        public async Task<PaylikeApiResponse<PaylikeModels.Transaction>> Capture(string transactionId, int amount, string currency, string descriptor)
        {
            var source = new { Amount = amount, Currency = currency, Descriptor = descriptor };
            return await _client.MakePaylikeApiRequest<PaylikeModels.Transaction>($"transactions/{transactionId}/captures", source);
        }
        public async Task<PaylikeApiResponse<PaylikeModels.Transaction>> Capture(string transactionId, decimal amount, string currency, string descriptor)
        {
            return await Capture(transactionId, amount.ToMinorUnits(currency), currency, descriptor);
        }

        public async Task<PaylikeApiResponse<List<PaylikeModels.Transaction>>> GetRecent(string merchantId, int limit)
        {
            return await _client.MakePaylikeApiRequest<List<PaylikeModels.Transaction>>($"merchants/{merchantId}/transactions?limit={limit}");
        }
        public async Task<PaylikeApiResponse<PaylikeModels.Transaction>> Refund(string transactionId, string descriptor, int amountAsMinor)
        {
            var source = new { descriptor = descriptor, amount = amountAsMinor };
            return await _client.MakePaylikeApiRequest<PaylikeModels.Transaction>($"transactions/{transactionId}/refunds", source);
        }
        public async Task<PaylikeApiResponse<PaylikeModels.Transaction>> Refund(string transactionId, string descriptor, string currency, decimal amountAsMajor)
        {
            return await Refund(transactionId, descriptor, amountAsMajor.ToMinorUnits(currency));
        }
    }
}
