﻿namespace Application.Paylike.Paylike.Clients.PaylikeModels
{
    public class CurrencyData
    {
        public string Code { get; set; }
        public string Currency { get; set; }
        public string Numeric { get; set; }
        public int Exponent { get; set; }
    }
}