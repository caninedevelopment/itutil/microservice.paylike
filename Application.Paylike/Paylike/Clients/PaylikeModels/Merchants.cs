﻿namespace Application.Paylike.Paylike.Clients.PaylikeModels
{
    using System.Collections.Generic;

    public class AllMerchants
    {
        public List<Merchant> Merchants { get; set; }
    }
}
