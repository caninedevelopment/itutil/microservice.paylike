﻿namespace Application.Paylike.Paylike.Clients.PaylikeModels
{
    public class Company
    {
        public string Country { get; set; }

        public string Number { get; set; }

        public Company() { }

        public Company(DTO.CompanyDTO c)
        {
            this.Country = c.Country;
            this.Number = c.Number;
        }
    }
}
