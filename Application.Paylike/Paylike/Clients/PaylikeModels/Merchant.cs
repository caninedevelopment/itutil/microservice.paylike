﻿namespace Application.Paylike.Paylike.Clients.PaylikeModels
{
    public class Merchant
    {
        // Id and key info

        /// <summary>
        /// Set or get Id, information Paylike set
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Set or get key, information paylike set
        /// </summary>
        public string Key { get; set; }

        // All under for create

        public string Name { get; set; }

        public string Currency { get; set; }

        public bool Test { get; set; }

        public string Email { get; set; }

        public string Website { get; set; }

        public string Descriptor { get; set; }

        public Company Company { get; set; }

        public Bank Bank { get; set; }

        public Merchant() { }

        public Merchant(DTO.InsertMerchantDTO merchant)
        {
            this.Name = merchant.Name;
            this.Currency = merchant.Currency;
            this.Test = merchant.Test;
            this.Email = merchant.Email;
            this.Website = merchant.Website;
            this.Descriptor = merchant.Descriptor;
            this.Company = new Company(merchant.Company);
            this.Bank = new Bank(merchant.Bank);
        }
    }
}
