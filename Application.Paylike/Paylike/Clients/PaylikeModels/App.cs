﻿namespace Application.Paylike.Paylike.Clients.PaylikeModels
{
    using Newtonsoft.Json;

    public class App
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Key { get; set; }
    }
}
