﻿namespace Application.Paylike.Paylike.Clients.PaylikeModels
{
    public class Fee
    {
        public int Flat { get; set; }
        public int Rate { get; set; }
    }
}