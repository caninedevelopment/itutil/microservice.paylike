﻿namespace Application.Paylike.Paylike.Clients.PaylikeModels
{
    public class Bank
    {
        public string IBAN { get; set; }

        public Bank() { }

        public Bank(DTO.BankDTO bank)
        {
            this.IBAN = bank.IBAN;
        }
    }
}