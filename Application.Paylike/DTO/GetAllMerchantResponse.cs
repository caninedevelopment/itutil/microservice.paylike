﻿namespace Application.Paylike.DTO
{
    using Application.Paylike.Paylike.Clients.PaylikeModels;
    using System.Collections.Generic;
    using System.Linq;

    public class GetAllMerchantResponse
    {
        public List<GetMerchantDTO> Merchants { get; set; }

        public GetAllMerchantResponse(List<Merchant> result)
        {
            this.Merchants = result.Select(x => new GetMerchantDTO(x)).ToList();
        }
    }
}
