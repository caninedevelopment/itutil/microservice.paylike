﻿namespace Application.Paylike.DTO
{
    using Application.Paylike.Paylike.Clients.PaylikeModels;
    using System.Collections.Generic;
    using System.Linq;

    public class GetAllTransactionsResponse
    {
        public List<GetTransactionDTO> Transactions { get; set; }

        public GetAllTransactionsResponse(List<Transaction> result)
        {
            this.Transactions = result.Select(x => new GetTransactionDTO(x)).ToList();
        }
    }
}
