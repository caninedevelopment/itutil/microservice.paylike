﻿namespace Application.Paylike.DTO
{
    using Application.Paylike.Paylike.Clients.PaylikeModels;

    public class BankDTO
    {
        public string IBAN { get; set; }

        public BankDTO() { }

        public BankDTO(Bank b)
        {
            this.IBAN = b.IBAN;
        }
    }
}
