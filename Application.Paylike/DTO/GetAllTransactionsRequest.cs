﻿namespace Application.Paylike.DTO
{
    public class GetAllTransactionsRequest
    {
        public string AppKey { get; set; }
        public string MerchantId { get; set; }
        public int? Limit { get; set; }
    }
}
