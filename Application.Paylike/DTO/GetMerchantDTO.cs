﻿namespace Application.Paylike.DTO
{
    using Application.Paylike.Paylike.Clients.PaylikeModels;

    public class GetMerchantDTO : BaseMerchantDTO
    {
        /// <summary>
        /// Set or get Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Set or get key
        /// </summary>
        public string Key { get; set; }

        public GetMerchantDTO(Merchant m)
        {
            this.Id = m.Id;
            this.Key = m.Key;
            this.Name = m.Name;
            this.Test = m.Test;
            this.Website = m.Website;
            this.Email = m.Email;
            this.Descriptor = m.Descriptor;
            this.Currency = m.Currency;
            this.Company = new CompanyDTO(m.Company);
            this.Bank = new BankDTO(m.Bank);
        }
    }
}
