﻿namespace Application.Paylike.DTO
{
    public class SaveCardDTO
    {
        /// <summary>
        /// Set or get transaction Id
        /// </summary>
        public string TransactionId { get; set; }

        /// <summary>
        /// Set or get merchant id
        /// </summary>
        public string MerchantId { get; set; }
    }
}
