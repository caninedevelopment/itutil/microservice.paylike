﻿namespace Application.Paylike.DTO
{
    public class GetAllMerchantDTO
    {
        public string AppKey { get; set; }
        public string AppId { get; set; }
        public int Skip { get; set; }
        public int?  Limit { get; set; }
    }
}
