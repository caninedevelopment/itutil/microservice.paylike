﻿namespace Application.Paylike.DTO
{
    using Application.Paylike.Paylike.Clients.PaylikeModels;

    public class CompanyDTO
    {
        public string Country { get; set; }

        public string Number { get; set; }

        public CompanyDTO() { }

        public CompanyDTO(Company c)
        {
            this.Country = c.Country;
            this.Number = c.Number;
        }
    }
}
