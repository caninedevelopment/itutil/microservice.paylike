﻿namespace Application.Paylike.DTO
{
    public class GetCardDTO
    {
        /// <summary>
        /// Set or get app key
        /// </summary>
        public string AppKey { get; set; }

        /// <summary>
        /// Get or set card id
        /// </summary>
        public string CardId { get; set; }
    }
}
