﻿

namespace Application.Paylike.DTO
{
    public class InsertTransactionDTO
    {
        /// <summary>
        /// Get or set App key
        /// </summary>
        public string AppKey { get; set; }

        /// <summary>
        /// Get or set MerchantId
        /// </summary>
        public string MerchantId { get; set; }

        /// <summary>
        /// Get or set CardId
        /// </summary>
        public string CardId { get; set; }

        /// <summary>
        /// Get or set Descriptor
        /// </summary>
        public string Descriptor { get; set; }

        /// <summary>
        /// Get or set Currency
        /// </summary>
        public string Currency { get; set; }

        /// <summary>
        /// Get or set amount as minor
        /// </summary>
        public int AmountAsMinor { get; set; }
    }
}
