﻿namespace Application.Paylike.DTO
{
    public class BaseMerchantDTO
    {
        /// <summary>
        /// Set or get name of merchant
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Set or get currency
        /// </summary>
        public string Currency { get; set; }

        /// <summary>
        /// Set or get test, if its a test merchant
        /// </summary>
        public bool Test { get; set; }

        /// <summary>
        /// Set or get Email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Set or get Website 
        /// </summary>
        public string Website { get; set; }

        /// <summary>
        /// Set or get Descriptor
        /// </summary>
        public string Descriptor { get; set; }

        /// <summary>
        /// Set or get Company
        /// </summary>
        public CompanyDTO Company { get; set; }

        /// <summary>
        /// Set or get Bank
        /// </summary>
        public BankDTO Bank { get; set; }
    }
}
