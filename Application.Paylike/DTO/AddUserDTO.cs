﻿namespace Application.Paylike.DTO
{
    public class AddUserDTO
    {
        /// <summary>
        /// Set or get Email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Set or get Merchant id
        /// </summary>
        public string MerchantId { get; set; }
    }
}
