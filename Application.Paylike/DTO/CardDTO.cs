﻿namespace Application.Paylike.DTO
{
    using Application.Paylike.Paylike.Clients.PaylikeModels;
    using System;

    public class CardDTO
    {

        /// <summary>
        /// Set or get car id
        /// </summary>
        public string CardId { get; set; }

        /// <summary>
        /// Set or get last 4 digits of card number
        /// </summary>
        public string Last4 { get; set; }

        /// <summary>
        /// Set or get scheme. (Ect. visa, master)
        /// </summary>
        public string Scheme { get; set; }

        /// <summary>
        /// Set or get expire date of card
        /// </summary>
        public DateTime ExpireDate { get; set; }

        public CardDTO() { }

        public CardDTO(Card card)
        {
            this.CardId = card.Id;
            this.Last4 = card.Last4;
            this.Scheme = card.Scheme;
            this.ExpireDate = card.Expiry;
        }
    }
}
