﻿namespace Application.Paylike.DTO
{
    public class InsertMerchantDTO : BaseMerchantDTO
    {
        /// <summary>
        /// Set or get app key
        /// </summary>
        public string AppKey { get; set; }
    }
}
