﻿namespace Application.Paylike.DTO
{
    using Application.Paylike.Paylike.Clients.PaylikeModels;

    public class GetTransactionDTO
    {
        /// <summary>
        /// Set or get transaction id
        /// </summary>
        public string TransactionId { get; set; }

        public GetTransactionDTO(Transaction trans)
        {
            this.TransactionId = trans.Id;
        }
    }
}
