﻿namespace Application.Paylike
{
    using System;
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Open Paylike.");
            ITUtil.Common.Console.Program.StartRabbitMq(new System.Collections.Generic.List<ITUtil.Common.Command.INamespace>()
            {
                //new Commands.PaylikeCard.Namespace(),
                new Commands.PaylikeMerchant.Namespace(),
                new Commands.PaylikeTransaction.Namespace()
            }, true);
        }
    }
}
